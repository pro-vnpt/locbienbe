package com.trandung.locbien.service;

import com.trandung.locbien.dto.ClassifyDTO;
import com.trandung.locbien.entity.BienSoEntity;
import com.trandung.locbien.repository.BienSoRepository;
import com.trandung.locbien.util.Const;
import com.trandung.locbien.util.SodepUtil;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class PlateService {

    private final BienSoRepository bienSoRepository;

    public List<BienSoEntity> getAll() {
        ZonedDateTime currentDay = ZonedDateTime.now(Const.ZONE);
        return bienSoRepository.findAllByRegisterToTimeBetween(currentDay.withHour(0).withMinute(0).withSecond(0).withNano(0), currentDay.plusDays(3).withHour(0).withMinute(0).withSecond(0).withNano(0));

    }

    public Page<BienSoEntity> getAllPaging(Pageable pageable) {
        return bienSoRepository.findAll(pageable);
    }

    public List<ClassifyDTO> classify(List<String> plates) {
        return plates.stream().map(i-> {
            String dang_so = SodepUtil.getDangSo(i);
            return new ClassifyDTO(i, dang_so);
        }).collect(Collectors.toList());
    }
}
