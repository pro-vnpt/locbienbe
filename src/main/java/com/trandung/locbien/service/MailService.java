package com.trandung.locbien.service;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.request.SendDocument;
import com.pengrad.telegrambot.response.SendResponse;
import com.trandung.locbien.entity.BienSoEntity;
import com.trandung.locbien.repository.BienSoRepository;
import com.trandung.locbien.repository.UserRepository;
import com.trandung.locbien.util.Const;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jobrunr.jobs.annotations.Job;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Slf4j
public class MailService {

//    private final JavaMailSender emailSender;

    private final BienSoRepository bienSoRepository;

    private final UserRepository userRepository;

    private static String BOT_TOKEN = "6594084143:AAHt1mQjpWwdf6fcY33kt8nAkiUYIS4bWyw";
    private static String CHAT_ID = "-4136822286";

    @Job(name = "The email job to all user bid today", retries = 1)
    public void sendToAllBidToday() throws InterruptedException, IOException {
        ZonedDateTime now = ZonedDateTime.now(Const.ZONE);
        List<BienSoEntity> bienSoEntities = bienSoRepository.findAllByAuctionStartTimeBetween(now.withHour(0).withMinute(0).withSecond(0).withNano(0), now.plusDays(1).withHour(0).withMinute(0).withSecond(0).withNano(0));
        if (bienSoEntities.isEmpty()) return;
        String content = bienSoEntities.stream().map(i -> i.getBks() + "\t" + i.getDanhGia()).collect(Collectors.joining("\n"));
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM");
        String caption = String.format("[%s] List biển đấu giá hôm nay", now.format(formatter));
        sendFile(BOT_TOKEN, CHAT_ID, caption, bienSoEntities.size() + " biển", content);
    }

    @Job(name = "The email job to all user expired next 3 day", retries = 1)
    public void sendToAllExpiredNext3Day() throws InterruptedException, IOException {
        ZonedDateTime now = ZonedDateTime.now(Const.ZONE);
        for (int i = 0; i < 3 ; i ++) {
            ZonedDateTime currentDay = now.plusDays(i);
            List<BienSoEntity> bienSoEntities = bienSoRepository.findAllByRegisterToTimeBetween(currentDay.withHour(0).withMinute(0).withSecond(0).withNano(0), currentDay.plusDays(1).withHour(0).withMinute(0).withSecond(0).withNano(0));
            if (bienSoEntities.isEmpty()) continue;
            String content = bienSoEntities.stream().map(b -> b.getBks() + "\t" + b.getDanhGia()).collect(Collectors.joining("\n"));
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM");
            String caption = String.format("[%s] List biển đến hạn ngày %s", now.format(formatter), currentDay.format(formatter));
            sendFile(BOT_TOKEN, CHAT_ID, caption, bienSoEntities.size() + " biển", content);
        }
    }

    @Job(name = "The email job to all user result today", retries = 1)
    public void sendToAllResultToday() throws InterruptedException, IOException {
        ZonedDateTime now = ZonedDateTime.now(Const.ZONE);
        List<BienSoEntity> bienSoEntities = bienSoRepository.findAllByAuctionStartTimeBetween(now.withHour(0).withMinute(0).withSecond(0).withNano(0), now.plusDays(1).withHour(0).withMinute(0).withSecond(0).withNano(0));
        if (bienSoEntities.isEmpty()) return;
        String content = bienSoEntities.stream().map(i -> {
            if (StringUtils.isNotEmpty(i.getGia())) {
                return i.getBks() + "(" + i.getGia().substring(0, i.getGia().length() - 6) + ")" + "\t" + i.getDanhGia();
            } else {
                return i.getBks() + "\t" + i.getDanhGia();
            }
        }).collect(Collectors.joining("\n"));
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM");

        String caption = String.format("[%s] Kết quả đấu giá hôm nay", now.format(formatter));
        sendFile(BOT_TOKEN, CHAT_ID, caption, bienSoEntities.size() + " biển", content);
    }

    @Job(name = "The email job to test user", retries = 1)
    public void sendToTest() throws InterruptedException {
//        ZonedDateTime now = ZonedDateTime.now(Const.ZONE);
//        List<BienSoEntity> bienSoEntities = bienSoRepository.findAllByAuctionStartTimeBetween(now.withHour(0).withMinute(0).withSecond(0).withNano(0), now.plusDays(1).withHour(0).withMinute(0).withSecond(0).withNano(0));
//        if (bienSoEntities.isEmpty()) return;
//        List<UserEntity> userEntities = Arrays.asList(new UserEntity(1,"","trandung.pvh@gmail.com",true));
//        String content = bienSoEntities.stream().map(i -> i.getBks() + "\t" + i.getDanhGia()).collect(Collectors.joining("\n"));
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM");
//        for (UserEntity user : userEntities) {
//            String text = "Xin chào , danh sách biển đấu giá ngày " + now.format(formatter) + ":\n" + content;
//            SimpleMailMessage message = new SimpleMailMessage();
//            message.setFrom("biensodep_service@hotmail.com");
//            message.setTo(user.getEmail());
//            message.setSubject(String.format("[%s] List biển đấu giá hôm nay", now.format(formatter)));
//            message.setText(text);
//            emailSender.send(message);
//            log.info("Send to {} ok", user.getEmail());
//            Thread.sleep(2000);
//        }
    }

    private void sendFile(String botToken, String chatId,String caption, String fileName, String content) throws IOException {
        // Create a temporary text file
        File file = File.createTempFile(fileName, ".txt");

        // Write content to the temporary file
        FileWriter writer = new FileWriter(file);
        writer.write(content);
        writer.close();

        TelegramBot bot = new TelegramBot(botToken);
        SendDocument sendDocument = new SendDocument(chatId, file).caption(caption).fileName(fileName + ".txt");

        // sync
        SendResponse sendResponse = bot.execute(sendDocument);
        boolean ok = sendResponse.isOk();

        log.info("Send file {} to group chat: {}", fileName, ok);
    }
}
