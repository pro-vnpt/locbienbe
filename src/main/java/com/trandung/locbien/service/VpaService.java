package com.trandung.locbien.service;

import com.aayushatharva.brotli4j.Brotli4jLoader;
import com.aayushatharva.brotli4j.decoder.Decoder;
import com.aayushatharva.brotli4j.decoder.DecoderJNI;
import com.aayushatharva.brotli4j.decoder.DirectDecompress;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.trandung.locbien.dto.BienSoDTO;
import com.trandung.locbien.dto.VpaResponseDTO;
import com.trandung.locbien.entity.BienSoEntity;
import com.trandung.locbien.repository.BienSoRepository;
import com.trandung.locbien.util.Const;
import com.trandung.locbien.util.SodepUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jobrunr.jobs.annotations.Job;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@AllArgsConstructor
public class VpaService {
    private final BienSoRepository bienSoRepository;

    @Job(name = "The job sync plate expired next 3 day", retries = 0)
    public void syncBienSoExpireNext3Day() throws URISyntaxException, IOException, InterruptedException {
        log.info("Start save plate expire next 3 day");
        bienSoRepository.saveAll(syncXeCon());
        bienSoRepository.saveAll(syncXeTai());
        log.info("End save plate");
    }

    private List<BienSoEntity> syncXeCon() throws URISyntaxException, IOException, InterruptedException {
        ObjectMapper objectMapper = new ObjectMapper();
        HttpClient client = HttpClient.newHttpClient();
        List<BienSoEntity> bienSoEntities = new ArrayList<>();
        DateTimeFormatter formatNgayDau = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        for (int j = 0; j < 3 ; j ++) {
            String ngayDau = ZonedDateTime.now(Const.ZONE).plusDays(j + 3).format(formatNgayDau);
            log.info("Start sync expire {}", ngayDau);
            String url = String.format("https://vpa.com.vn/api/v1/bks/search?page=%s&size=100&plate_status=waiting_auction&ma_loai_xe=1&start_date=%s&end_date=%s", 1, ngayDau, ngayDau);
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(new URI(url))
                    .header("User-Agent","PostmanRuntime/7.37.3")
                    .header("Accept-Encoding","gzip, deflate, br")
                    .header("Content-Type","application/json;charset=UTF-8")
                    .GET()
                    .build();
            HttpResponse<byte[]> response = client.send(request, HttpResponse.BodyHandlers.ofByteArray());
            String prettyData = decodeBrotli(response.body());
            VpaResponseDTO vpaResponseDTO;
            try{
                vpaResponseDTO = objectMapper.readValue(prettyData, VpaResponseDTO.class);
            } catch (Exception e) {
                vpaResponseDTO = new VpaResponseDTO();
                vpaResponseDTO.setTotal(0);
                vpaResponseDTO.setData(new ArrayList<>());
            }
            for(int i = 1; i <= (vpaResponseDTO.getTotal()/100) + 1 ; i ++) {
                url = String.format("https://vpa.com.vn/api/v1/bks/search?page=%s&size=100&plate_status=waiting_auction&ma_loai_xe=1&start_date=%s&end_date=%s", i, ngayDau, ngayDau);
                request = HttpRequest.newBuilder()
                        .uri(new URI(url))
                        .header("User-Agent","PostmanRuntime/7.37.3")
                        .header("Accept-Encoding","gzip, deflate, br")
                        .header("Content-Type","application/json;charset=UTF-8")
                        .GET()
                        .build();
                response = client.send(request, HttpResponse.BodyHandlers.ofByteArray());
                prettyData = decodeBrotli(response.body());
                try{
                    vpaResponseDTO = objectMapper.readValue(prettyData, VpaResponseDTO.class);
                } catch (Exception e) {
                    vpaResponseDTO = new VpaResponseDTO();
                    vpaResponseDTO.setTotal(0);
                    vpaResponseDTO.setData(new ArrayList<>());
                }

                for (BienSoDTO dto : vpaResponseDTO.getData()) {
                    String dang_so = SodepUtil.getDangSo(dto.getBks());
                    if (StringUtils.isEmpty(dang_so)) continue;
                    BienSoEntity entity = BienSoEntity.builder()
                            .id(dto.getBks())
                            .bks(dto.getBks())
                            .danhGia(dang_so)
                            .loaiXe(dto.getLoaiXe().getNoi_dung())
                            .loaiBien(dto.getLoaiBien().stream().map(BienSoDTO.LoaiBien::getTen).collect(Collectors.joining(", ")))
                            .tinh(dto.getTinh().getTen())
                            .priority(dto.getLoaiBien().stream().min(Comparator.comparing(BienSoDTO.LoaiBien::getPriority)).orElse(new BienSoDTO.LoaiBien()).getPriority())
                            .auctionStartTime(ZonedDateTime.parse(dto.getAuctionStartTime(), DateTimeFormatter.ISO_DATE_TIME).withZoneSameInstant(Const.ZONE))
                            .auctionEndTime(ZonedDateTime.parse(dto.getAuctionEndTime(), DateTimeFormatter.ISO_DATE_TIME).withZoneSameInstant(Const.ZONE))
                            .registerFromTime(ZonedDateTime.parse(dto.getRegisterFromTime(), DateTimeFormatter.ISO_DATE_TIME).withZoneSameInstant(Const.ZONE))
                            .registerToTime(ZonedDateTime.parse(dto.getRegisterToTime(), DateTimeFormatter.ISO_DATE_TIME).withZoneSameInstant(Const.ZONE))
                            .createdDate(ZonedDateTime.now(Const.ZONE))
                            .build();
                    bienSoEntities.add(entity);
                }
            }
        }
        return bienSoEntities;
    }

    private List<BienSoEntity> syncXeTai() throws URISyntaxException, IOException, InterruptedException {
        ObjectMapper objectMapper = new ObjectMapper();
        HttpClient client = HttpClient.newHttpClient();
        List<BienSoEntity> bienSoEntities = new ArrayList<>();
        DateTimeFormatter formatNgayDau = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        for (int j = 0; j < 3 ; j ++) {
            String ngayDau = ZonedDateTime.now(Const.ZONE).plusDays(j + 3).format(formatNgayDau);
            log.info("Start sync expire {}", ngayDau);
            String url = String.format("https://vpa.com.vn/api/v1/bks/search?page=%s&size=100&plate_status=waiting_auction&ma_loai_xe=3&start_date=%s&end_date=%s", 1, ngayDau, ngayDau);
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(new URI(url))
                    .header("User-Agent","PostmanRuntime/7.37.3")
                    .header("Accept-Encoding","gzip, deflate, br")
                    .header("Content-Type","application/json;charset=UTF-8")
                    .GET()
                    .build();
            HttpResponse<byte[]> response = client.send(request, HttpResponse.BodyHandlers.ofByteArray());
            String prettyData = decodeBrotli(response.body());
            VpaResponseDTO vpaResponseDTO;
            try{
                vpaResponseDTO = objectMapper.readValue(prettyData, VpaResponseDTO.class);
            } catch (Exception e) {
                vpaResponseDTO = new VpaResponseDTO();
                vpaResponseDTO.setTotal(0);
                vpaResponseDTO.setData(new ArrayList<>());
            }
            for(int i = 1; i <= (vpaResponseDTO.getTotal()/100) + 1 ; i ++) {
                url = String.format("https://vpa.com.vn/api/v1/bks/search?page=%s&size=100&plate_status=waiting_auction&ma_loai_xe=3&start_date=%s&end_date=%s", i, ngayDau, ngayDau);
                request = HttpRequest.newBuilder()
                        .uri(new URI(url))
                        .header("User-Agent","PostmanRuntime/7.37.3")
                        .header("Accept-Encoding","gzip, deflate, br")
                        .header("Content-Type","application/json;charset=UTF-8")
                        .GET()
                        .build();
                response = client.send(request, HttpResponse.BodyHandlers.ofByteArray());
                prettyData = decodeBrotli(response.body());
                try{
                    vpaResponseDTO = objectMapper.readValue(prettyData, VpaResponseDTO.class);
                } catch (Exception e) {
                    vpaResponseDTO = new VpaResponseDTO();
                    vpaResponseDTO.setTotal(0);
                    vpaResponseDTO.setData(new ArrayList<>());
                }

                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                for (BienSoDTO dto : vpaResponseDTO.getData()) {
                    String dang_so = SodepUtil.getDangSo(dto.getBks());
                    if (StringUtils.isEmpty(dang_so)) continue;
                    BienSoEntity entity = BienSoEntity.builder()
                            .id(dto.getBks())
                            .bks(dto.getBks())
                            .danhGia(dang_so)
                            .loaiXe(dto.getLoaiXe().getNoi_dung())
                            .loaiBien(dto.getLoaiBien().stream().map(BienSoDTO.LoaiBien::getTen).collect(Collectors.joining(", ")))
                            .tinh(dto.getTinh().getTen())
                            .priority(dto.getLoaiBien().stream().min(Comparator.comparing(BienSoDTO.LoaiBien::getPriority)).orElse(new BienSoDTO.LoaiBien()).getPriority())
                            .auctionStartTime(ZonedDateTime.parse(dto.getAuctionStartTime(), DateTimeFormatter.ISO_DATE_TIME).withZoneSameInstant(Const.ZONE))
                            .auctionEndTime(ZonedDateTime.parse(dto.getAuctionEndTime(), DateTimeFormatter.ISO_DATE_TIME).withZoneSameInstant(Const.ZONE))
                            .registerFromTime(ZonedDateTime.parse(dto.getRegisterFromTime(), DateTimeFormatter.ISO_DATE_TIME).withZoneSameInstant(Const.ZONE))
                            .registerToTime(ZonedDateTime.parse(dto.getRegisterToTime(), DateTimeFormatter.ISO_DATE_TIME).withZoneSameInstant(Const.ZONE))
                            .createdDate(ZonedDateTime.now(Const.ZONE))
                            .build();
                    bienSoEntities.add(entity);
                }
            }
        }
        return bienSoEntities;
    }

    @Job(name = "The job sync plate expired today", retries = 0)
    public void syncBienSoBidToday() throws URISyntaxException, IOException, InterruptedException {
        log.info("Start save plate bid today");
        bienSoRepository.saveAll(syncBidToDayXeCon());
        bienSoRepository.saveAll(syncBidToDayXeTai());
        log.info("End save plate");
    }

    private List<BienSoEntity> syncBidToDayXeCon() throws URISyntaxException, IOException, InterruptedException {
        ObjectMapper objectMapper = new ObjectMapper();
        HttpClient client = HttpClient.newHttpClient();
        List<BienSoEntity> bienSoEntities = new ArrayList<>();
        DateTimeFormatter formatNgayDau = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String ngayDau = ZonedDateTime.now(Const.ZONE).format(formatNgayDau);
        String url = String.format("https://vpa.com.vn/api/v1/bks/search?page=%s&size=100&plate_status=waiting_auction&ma_loai_xe=1&start_date=%s&end_date=%s", 1, ngayDau, ngayDau);
        HttpRequest request = HttpRequest.newBuilder()
                .uri(new URI(url))
                .header("User-Agent","PostmanRuntime/7.37.3")
                .header("Accept-Encoding","gzip, deflate, br")
                .header("Content-Type","application/json;charset=UTF-8")
                .GET()
                .build();
        HttpResponse<byte[]> response = client.send(request, HttpResponse.BodyHandlers.ofByteArray());
        String prettyData = decodeBrotli(response.body());
        VpaResponseDTO vpaResponseDTO;
        try{
            vpaResponseDTO = objectMapper.readValue(prettyData, VpaResponseDTO.class);
        } catch (Exception e) {
            vpaResponseDTO = new VpaResponseDTO();
            vpaResponseDTO.setTotal(0);
            vpaResponseDTO.setData(new ArrayList<>());
        }
        for(int i = 1; i <= (vpaResponseDTO.getTotal()/100) + 1 ; i ++) {
            url = String.format("https://vpa.com.vn/api/v1/bks/search?page=%s&size=100&plate_status=waiting_auction&ma_loai_xe=1&start_date=%s&end_date=%s", i, ngayDau, ngayDau);
            request = HttpRequest.newBuilder()
                    .uri(new URI(url))
                    .header("User-Agent","PostmanRuntime/7.37.3")
                    .header("Accept-Encoding","gzip, deflate, br")
                    .header("Content-Type","application/json;charset=UTF-8")
                    .GET()
                    .build();
            response = client.send(request, HttpResponse.BodyHandlers.ofByteArray());
            prettyData = decodeBrotli(response.body());
            try{
                vpaResponseDTO = objectMapper.readValue(prettyData, VpaResponseDTO.class);
            } catch (Exception e) {
                vpaResponseDTO = new VpaResponseDTO();
                vpaResponseDTO.setTotal(0);
                vpaResponseDTO.setData(new ArrayList<>());
            }

            for (BienSoDTO dto : vpaResponseDTO.getData()) {
                String dang_so = SodepUtil.getDangSo(dto.getBks());
                if (StringUtils.isEmpty(dang_so)) continue;
                BienSoEntity entity = BienSoEntity.builder()
                        .id(dto.getBks())
                        .bks(dto.getBks())
                        .danhGia(dang_so)
                        .loaiXe(dto.getLoaiXe().getNoi_dung())
                        .loaiBien(dto.getLoaiBien().stream().map(BienSoDTO.LoaiBien::getTen).collect(Collectors.joining(", ")))
                        .tinh(dto.getTinh().getTen())
                        .priority(dto.getLoaiBien().stream().min(Comparator.comparing(BienSoDTO.LoaiBien::getPriority)).orElse(new BienSoDTO.LoaiBien()).getPriority())
                        .auctionStartTime(ZonedDateTime.parse(dto.getAuctionStartTime(), DateTimeFormatter.ISO_DATE_TIME).withZoneSameInstant(Const.ZONE))
                        .auctionEndTime(ZonedDateTime.parse(dto.getAuctionEndTime(), DateTimeFormatter.ISO_DATE_TIME).withZoneSameInstant(Const.ZONE))
                        .registerFromTime(ZonedDateTime.parse(dto.getRegisterFromTime(), DateTimeFormatter.ISO_DATE_TIME).withZoneSameInstant(Const.ZONE))
                        .registerToTime(ZonedDateTime.parse(dto.getRegisterToTime(), DateTimeFormatter.ISO_DATE_TIME).withZoneSameInstant(Const.ZONE))
                        .createdDate(ZonedDateTime.now(Const.ZONE))
                        .build();
                bienSoEntities.add(entity);
            }
        }
        return bienSoEntities;
    }

    private List<BienSoEntity> syncBidToDayXeTai() throws URISyntaxException, IOException, InterruptedException {
        ObjectMapper objectMapper = new ObjectMapper();
        HttpClient client = HttpClient.newHttpClient();
        List<BienSoEntity> bienSoEntities = new ArrayList<>();
        DateTimeFormatter formatNgayDau = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String ngayDau = ZonedDateTime.now(Const.ZONE).format(formatNgayDau);
        String url = String.format("https://vpa.com.vn/api/v1/bks/search?page=%s&size=100&plate_status=waiting_auction&ma_loai_xe=3&start_date=%s&end_date=%s", 1, ngayDau, ngayDau);
        HttpRequest request = HttpRequest.newBuilder()
                .uri(new URI(url))
                .header("User-Agent","PostmanRuntime/7.37.3")
                .header("Accept-Encoding","gzip, deflate, br")
                .header("Content-Type","application/json;charset=UTF-8")
                .GET()
                .build();
        HttpResponse<byte[]> response = client.send(request, HttpResponse.BodyHandlers.ofByteArray());
        String prettyData = decodeBrotli(response.body());
        VpaResponseDTO vpaResponseDTO;
        try{
            vpaResponseDTO = objectMapper.readValue(prettyData, VpaResponseDTO.class);
        } catch (Exception e) {
            vpaResponseDTO = new VpaResponseDTO();
            vpaResponseDTO.setTotal(0);
            vpaResponseDTO.setData(new ArrayList<>());
        }
        for(int i = 1; i <= (vpaResponseDTO.getTotal()/100) + 1 ; i ++) {
            url = String.format("https://vpa.com.vn/api/v1/bks/search?page=%s&size=100&plate_status=waiting_auction&ma_loai_xe=1&start_date=%s&end_date=%s", i, ngayDau, ngayDau);
            request = HttpRequest.newBuilder()
                    .uri(new URI(url))
                    .header("User-Agent","PostmanRuntime/7.37.3")
                    .header("Accept-Encoding","gzip, deflate, br")
                    .header("Content-Type","application/json;charset=UTF-8")
                    .GET()
                    .build();
            response = client.send(request, HttpResponse.BodyHandlers.ofByteArray());
            prettyData = decodeBrotli(response.body());
            try{
                vpaResponseDTO = objectMapper.readValue(prettyData, VpaResponseDTO.class);
            } catch (Exception e) {
                vpaResponseDTO = new VpaResponseDTO();
                vpaResponseDTO.setTotal(0);
                vpaResponseDTO.setData(new ArrayList<>());
            }

            for (BienSoDTO dto : vpaResponseDTO.getData()) {
                String dang_so = SodepUtil.getDangSo(dto.getBks());
                if (StringUtils.isEmpty(dang_so)) continue;
                BienSoEntity entity = BienSoEntity.builder()
                        .id(dto.getBks())
                        .bks(dto.getBks())
                        .danhGia(dang_so)
                        .loaiXe(dto.getLoaiXe().getNoi_dung())
                        .loaiBien(dto.getLoaiBien().stream().map(BienSoDTO.LoaiBien::getTen).collect(Collectors.joining(", ")))
                        .tinh(dto.getTinh().getTen())
                        .priority(dto.getLoaiBien().stream().min(Comparator.comparing(BienSoDTO.LoaiBien::getPriority)).orElse(new BienSoDTO.LoaiBien()).getPriority())
                        .auctionStartTime(ZonedDateTime.parse(dto.getAuctionStartTime(), DateTimeFormatter.ISO_DATE_TIME).withZoneSameInstant(Const.ZONE))
                        .auctionEndTime(ZonedDateTime.parse(dto.getAuctionEndTime(), DateTimeFormatter.ISO_DATE_TIME).withZoneSameInstant(Const.ZONE))
                        .registerFromTime(ZonedDateTime.parse(dto.getRegisterFromTime(), DateTimeFormatter.ISO_DATE_TIME).withZoneSameInstant(Const.ZONE))
                        .registerToTime(ZonedDateTime.parse(dto.getRegisterToTime(), DateTimeFormatter.ISO_DATE_TIME).withZoneSameInstant(Const.ZONE))
                        .createdDate(ZonedDateTime.now(Const.ZONE))
                        .build();
                bienSoEntities.add(entity);
            }
        }
        return bienSoEntities;
    }

    @Job(name = "The job sync result daily", retries = 0)
    public void syncKetQua() throws URISyntaxException, IOException, InterruptedException {
        log.info("The job sync result daily");
        ZonedDateTime now = ZonedDateTime.now(Const.ZONE);
        List<BienSoEntity> bienSoEntities = bienSoRepository.findAllByGiaIsNullAndAuctionStartTimeBetween(now.withHour(0).withMinute(0).withSecond(0).withNano(0), now.plusDays(1).withHour(0).withMinute(0).withSecond(0).withNano(0));
        if (bienSoEntities.isEmpty()) {
            log.info("List plate empty");
            return;
        }
        HttpClient client = HttpClient.newHttpClient();
        for (BienSoEntity e : bienSoEntities) {
            String url = String.format("https://vpa.com.vn/api/v1/bidding-result?page=1&size=21&q=%s", e.getBks().toLowerCase());
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(new URI(url))
                    .header("User-Agent","PostmanRuntime/7.37.3")
                    .header("Accept-Encoding","gzip, deflate, br")
                    .header("Content-Type","application/json;charset=UTF-8")
                    .GET()
                    .build();
            HttpResponse<byte[]> response = client.send(request, HttpResponse.BodyHandlers.ofByteArray());
            String prettyData = decodeBrotli(response.body());
            String gia = StringUtils.substringBetween(prettyData, "gia_dau_gia\":", ",");
            if (StringUtils.isNotBlank(gia)) e.setGia(gia);
        }
        log.info("Start save plate price");
        bienSoRepository.saveAll(bienSoEntities);
        log.info("End save plate price");
    }

    private static String decodeBrotli(byte[] data) throws IOException {
        // Load the native library
        Brotli4jLoader.ensureAvailability();
        DirectDecompress directDecompress = Decoder.decompress(data);
        if (directDecompress.getResultStatus() == DecoderJNI.Status.DONE) {
            System.out.println("Decompression Successful: " + new String(directDecompress.getDecompressedData()));
            return new String(directDecompress.getDecompressedData());
        } else {
            System.out.println("Some Error Occurred While Decompressing");
            return "";
        }
    }
}
