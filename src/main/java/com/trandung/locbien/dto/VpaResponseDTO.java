package com.trandung.locbien.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class VpaResponseDTO {
    private List<BienSoDTO> data;
    private Integer total;
}
