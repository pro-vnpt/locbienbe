package com.trandung.locbien.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class BienSoDTO {
    private String id;
    private String bks;
    private String ngay_dau_gia;
    private Boolean du_kien_dau_gia;
    private String gia_dau_gia;
    private Tinh tinh;
    @JsonProperty("loai_bien")
    private List<LoaiBien> loaiBien;
    @JsonProperty("loai_xe")
    private LoaiXe loaiXe;

    private String auctionStartTime;
    private String auctionEndTime;
    private String registerFromTime;
    private String registerToTime;
    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    public class Tinh {
        public Integer id;
        public String ma;
        public String ten;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class LoaiBien {
        public Integer id;
        public String ten;
        public Integer priority;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    public class LoaiXe {
        public Integer id;
        public String ma;
        public String noi_dung;
    }
}
