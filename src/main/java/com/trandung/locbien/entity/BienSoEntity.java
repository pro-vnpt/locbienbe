package com.trandung.locbien.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.ZonedDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "bien_so")
@Entity
@Builder
public class BienSoEntity {
    @Id
    private String id; // tự động gen

    @Column(name = "bks")
    private String bks;

    @Column(name = "loai_xe")
    private String loaiXe;

    @Column(name = "loai_bien")
    private String loaiBien;

    @Column(name = "tinh")
    private String tinh;

    @Column(name = "priority")
    private Integer priority;

    @Column(name = "auction_start_time")
    private ZonedDateTime auctionStartTime;

    @Column(name = "auction_end_time")
    private ZonedDateTime auctionEndTime;

    @Column(name = "register_from_time")
    private ZonedDateTime registerFromTime;

    @Column(name = "register_to_time")
    private ZonedDateTime registerToTime;

    @Column(name = "danh_gia")
    private String danhGia;

    @Column(name = "gia")
    private String gia;

    @Column(name = "created_date")
    private ZonedDateTime createdDate;
}
