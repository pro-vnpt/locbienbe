package com.trandung.locbien.util;

import org.apache.commons.lang3.StringUtils;

import java.util.*;

public class SodepUtil {
    public static String getPlate(String data) {
        return data.substring(data.length() - 5);
    }

    public static String getDangSo(String data) {
        List<String> dang_so = new ArrayList<>();
        if (loi_phong_thuy(data)) return null;
        if (ngu_quy(data)) dang_so.add("ngu_quy");
        if (tu_quy_cuoi(data)) dang_so.add("tu_quy_cuoi");
        if (tu_quy_giua(data)) dang_so.add("tu_quy_giua");
        if (tam_hoa_cuoi((data))) dang_so.add("tam_hoa_cuoi");
        if (tam_hoa_giua((data))) dang_so.add("tam_hoa_giua");
        if (loc_phat_5((data))) dang_so.add("loc_phat_5");
        if (loc_phat_4((data))) dang_so.add("loc_phat_4");
        if (loc_phat_3((data))) dang_so.add("loc_phat_3");

        if (than_tai5_79((data))) dang_so.add("than_tai5_79");
        if (than_tai4_79((data))) dang_so.add("than_tai4_79");
        if (than_tai3_79((data))) dang_so.add("than_tai3_79");

//        if (than_tai5_59((data))) dang_so.add("than_tai5_59");
//        if (than_tai4_59((data))) dang_so.add("than_tai4_59");
//        if (than_tai3_59((data))) dang_so.add("than_tai3_59");

//        if (than_tai5_39((data))) dang_so.add("than_tai5_39");
//        if (than_tai4_39((data))) dang_so.add("than_tai4_39");
//        if (than_tai3_39((data))) dang_so.add("than_tai3_39");

//        if (than_tai5_19((data))) dang_so.add("than_tai5_19");
//        if (than_tai4_19((data))) dang_so.add("than_tai4_19");
//        if (than_tai3_19((data))) dang_so.add("than_tai3_19");

        if (sanh_tien_5(data)) dang_so.add("sanh_tien_5");
        if (sanh_tien_4(data)) dang_so.add("sanh_tien_4");
        if (sanh_tien_3(data)) dang_so.add("sanh_tien_3");

        if (sanh_tien_chan_le(data)) dang_so.add("sanh_tien_chan_le");
        if (lap_ganh_vip(data)) dang_so.add("lap_ganh_vip");
//        if (lap_ganh(data)) dang_so.add("lap_ganh");
//        if (tong_9_khong_loi(data)) dang_so.add("tong_9");
        if (duoi_dep(data)) dang_so.add("duoi_dep");
        if(!dang_so.isEmpty()) return String.join(" ", dang_so);
        return null;
    }
    public static boolean loc_phat_3(String data) {
        String plate = getPlate(data);
        String[] arr = plate.split("");
        List<String> last = Arrays.asList(arr[2], arr[3], arr[4]);
        boolean tam_hoa_cuoi = tam_hoa_cuoi(data);
        boolean loc_phat_3 = last.stream().filter(i -> StringUtils.equals(i, "6")).count() + last.stream().filter(i -> StringUtils.equals(i, "8")).count() == 3;
        boolean loc_phat_4 = loc_phat_4(data);
        return !tam_hoa_cuoi && !loc_phat_4 && loc_phat_3;
    }

    public static boolean loc_phat_4(String data) {
        String plate = getPlate(data);
        String[] arr = plate.split("");
        List<String> last = Arrays.asList(arr[1], arr[2], arr[3], arr[4]);
        boolean tu_quy_cuoi = tu_quy_cuoi(data);
        boolean loc_phat_4 = last.stream().filter(i -> StringUtils.equals(i, "6")).count() + last.stream().filter(i -> StringUtils.equals(i, "8")).count() == 4;
        boolean loc_phat_5 = loc_phat_5(data);
        return !tu_quy_cuoi && !loc_phat_5 && loc_phat_4;
    }

    public static boolean loc_phat_5(String data) {
        String plate = getPlate(data);
        String[] arr = plate.split("");
        List<String> last = Arrays.asList(arr);
        boolean ngu_quy = ngu_quy(data);
        boolean loc_phat_5 = last.stream().filter(i -> StringUtils.equals(i, "6")).count() + last.stream().filter(i -> StringUtils.equals(i, "8")).count() == 5;
        return !ngu_quy && loc_phat_5;
    }

    public static boolean tam_hoa_cuoi(String data) {
        String plate = getPlate(data);
        String[] arr = plate.split("");
        Set<String> set = new HashSet<>(Arrays.asList(arr[2], arr[3], arr[4]));
        return !tu_quy_cuoi(data) && set.size() == 1 && !set.contains("0") && !set.contains("1") && !set.contains("4");
    }

    public static boolean tam_hoa_giua(String data) {
        String plate = getPlate(data);
        List<String> list = Arrays.asList("222", "333", "555", "666", "777", "888", "999");
        for (String l : list) {
            if (plate.contains(l) && !tam_hoa_cuoi(data)) return true;
        }
        return false;
    }

    public static boolean tu_quy_cuoi(String data) {
        String plate = getPlate(data);
        String[] arr = plate.split("");
        Set<String> set = new HashSet<>(Arrays.asList(arr[1], arr[2], arr[3], arr[4]));
        return !ngu_quy(data) && set.size() == 1;
    }

    public static boolean tu_quy_giua(String data) {
        String plate = getPlate(data);
        List<String> list = Arrays.asList("0000", "1111", "2222", "3333", "4444", "5555", "6666", "7777", "8888", "9999");
        for (String l : list) {
            if (plate.contains(l) && !tu_quy_cuoi(data)) return true;
        }
        return false;
    }

    public static boolean ngu_quy(String data) {
        String plate = getPlate(data);
        String[] arr = plate.split("");
        Set<String> set = new HashSet<>(Arrays.asList(arr[0], arr[1], arr[2], arr[3], arr[4]));
        return set.size() == 1;
    }

    public static boolean than_tai3_19(String data) {
        String plate = getPlate(data);
        String[] arr = plate.split("");
        List<String> last = Arrays.asList(arr[2], arr[3], arr[4]);
        return !than_tai4_19(data) && last.contains("1") && last.contains("9") && last.stream().filter(i -> Objects.equals(i, "1") || Objects.equals(i, "9")).count() == 3;
    }

    public static boolean than_tai4_19(String data) {
        String plate = getPlate(data);
        String[] arr = plate.split("");
        List<String> last = Arrays.asList(arr[1], arr[2], arr[3], arr[4]);
        return !than_tai5_19(data) && last.contains("1") && last.contains("9") && last.stream().filter(i -> Objects.equals(i, "1") || Objects.equals(i, "9")).count() == 4;
    }

    public static boolean than_tai5_19(String data) {
        String plate = getPlate(data);
        String[] arr = plate.split("");
        List<String> last = Arrays.asList(arr);
        return last.contains("1") && last.contains("9") && last.stream().filter(i -> Objects.equals(i, "1") || Objects.equals(i, "9")).count() == 5;
    }

    public static boolean than_tai3_39(String data) {
        String plate = getPlate(data);
        String[] arr = plate.split("");
        List<String> last = Arrays.asList(arr[2], arr[3], arr[4]);
        return !than_tai4_39(data) && last.contains("3") && last.contains("9") && last.stream().filter(i -> Objects.equals(i, "3") || Objects.equals(i, "9")).count() == 3;
    }

    public static boolean than_tai4_39(String data) {
        String plate = getPlate(data);
        String[] arr = plate.split("");
        List<String> last = Arrays.asList(arr[1], arr[2], arr[3], arr[4]);
        return !than_tai5_39(data) && last.contains("3") && last.contains("9") && last.stream().filter(i -> Objects.equals(i, "3") || Objects.equals(i, "9")).count() == 4;
    }

    public static boolean than_tai5_39(String data) {
        String plate = getPlate(data);
        String[] arr = plate.split("");
        List<String> last = Arrays.asList(arr);
        return last.contains("3") && last.contains("9") && last.stream().filter(i -> Objects.equals(i, "3") || Objects.equals(i, "9")).count() == 5;
    }

    public static boolean than_tai3_59(String data) {
        String plate = getPlate(data);
        String[] arr = plate.split("");
        List<String> last = Arrays.asList(arr[2], arr[3], arr[4]);
        return !than_tai4_59(data) && last.contains("5") && last.contains("9") && last.stream().filter(i -> Objects.equals(i, "5") || Objects.equals(i, "9")).count() == 3;
    }

    public static boolean than_tai4_59(String data) {
        String plate = getPlate(data);
        String[] arr = plate.split("");
        List<String> last = Arrays.asList(arr[1], arr[2], arr[3], arr[4]);
        return !than_tai5_59(data) && last.contains("5") && last.contains("9") && last.stream().filter(i -> Objects.equals(i, "5") || Objects.equals(i, "9")).count() == 4;
    }

    public static boolean than_tai5_59(String data) {
        String plate = getPlate(data);
        String[] arr = plate.split("");
        List<String> last = Arrays.asList(arr);
        return last.contains("5") && last.contains("9") && last.stream().filter(i -> Objects.equals(i, "5") || Objects.equals(i, "9")).count() == 5;
    }

    public static boolean than_tai3_79(String data) {
        String plate = getPlate(data);
        String[] arr = plate.split("");
        List<String> last = Arrays.asList(arr[2], arr[3], arr[4]);
        boolean than_tai_79 = (arr[3] + arr[4]).equals("79");
        return than_tai_79 && !than_tai4_79((data)) && last.stream().filter(i -> Objects.equals(i, "7") || Objects.equals(i, "9")).count() == 3;
    }

    public static boolean than_tai4_79(String data) {
        String plate = getPlate(data);
        String[] arr = plate.split("");
        List<String> last = Arrays.asList(arr[1], arr[2], arr[3], arr[4]);
        boolean than_tai_79 = (arr[3] + arr[4]).equals("79");
        return than_tai_79 && !than_tai5_79((data)) && last.stream().filter(i -> Objects.equals(i, "7") || Objects.equals(i, "9")).count() == 4;
    }

    public static boolean than_tai5_79(String data) {
        String plate = getPlate(data);
        String[] arr = plate.split("");
        List<String> last = Arrays.asList(arr);
        boolean than_tai_79 = (arr[3] + arr[4]).equals("79");
        return than_tai_79 && last.stream().filter(i -> Objects.equals(i, "7") || Objects.equals(i, "9")).count() == 5;
    }

    public static boolean tong_9_khong_loi(String data) {
        String plate = getPlate(data);
        String[] arr = plate.split("");
        boolean tong_9 = 9 == Integer.parseInt(arr[0]) + Integer.parseInt(arr[1]) + Integer.parseInt(arr[2]) + Integer.parseInt(arr[3]) + Integer.parseInt(arr[4]);
        boolean loi_phong_thuy = loi_phong_thuy(data);
        return tong_9 && !loi_phong_thuy;
    }

    public static boolean sanh_tien_3(String data) {
        String plate = getPlate(data);
        String[] arr = plate.split("");
        String last = arr[2] + arr[3] + arr[4];
        List<String> compare = Arrays.asList("012", "123", "234", "345", "456", "567", "678", "789");
        return compare.contains(last);
    }

    public static boolean sanh_tien_4(String data) {
        String plate = getPlate(data);
        String[] arr = plate.split("");
        String last = arr[1] + arr[2] + arr[3] + arr[4];
        List<String> compare = Arrays.asList("0123", "1234", "2345", "4567", "5678", "6789");
        return compare.contains(last);
    }

    public static boolean sanh_tien_5(String data) {
        String plate = getPlate(data);
        String[] arr = plate.split("");
        String last = arr[0] + arr[1] + arr[2] + arr[3] + arr[4];
        List<String> compare = Arrays.asList("12345", "23456", "34567", "45678", "56789");
        return compare.contains(last);
    }

    public static boolean sanh_tien_chan_le(String data) {
        String plate = getPlate(data);
        List<String> list = Arrays.asList("0246", "2468", "1357", "3579");
        for (String l : list) {
            if (plate.contains(l)) return true;
        }
        return false;
    }

    public static boolean lap_ganh(String data) {
        String plate = getPlate(data);
        String[] arr = plate.split("");
        boolean lap_ganh = (Objects.equals(arr[0], arr[2]) && Objects.equals(arr[1], arr[3])) || (Objects.equals(arr[0], arr[3]) && Objects.equals(arr[1], arr[4])) || (Objects.equals(arr[0], arr[4]) && Objects.equals(arr[1], arr[3])) || (Objects.equals(arr[1], arr[3]) && Objects.equals(arr[2], arr[4])) | (Objects.equals(arr[1], arr[4]) && arr[2] == arr[3]);
        return !tam_hoa_cuoi(data) && !lap_ganh_vip(data) && lap_ganh;
    }

    public static boolean lap_ganh_vip(String data) {
        String plate = getPlate(data);
        String[] arr = plate.split("");
        return !tam_hoa_cuoi(data) && (Objects.equals(arr[1], arr[3]) && Objects.equals(arr[2], arr[4]));
    }

    public static boolean loi_phong_thuy(String data) {
        String plate = getPlate(data);
        List<String> list = Arrays.asList("49", "53");
        for (String l : list) {
            if (plate.contains(l)) return true;
        }
        return false;
    }

    public static boolean duoi_dep(String data) {
        String plate = getPlate(data);
        String[] arr = plate.split("");
        String last = arr[3] + arr[4];
        List<String> list = Arrays.asList("66", "68", "86", "88", "89", "99");
        return list.contains(last) && !loi_phong_thuy(data) && !plate.contains("0") && !plate.contains("4");
    }
}
