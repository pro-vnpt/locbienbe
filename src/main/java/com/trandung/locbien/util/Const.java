package com.trandung.locbien.util;

import java.time.ZoneId;

public class Const {
    public final static ZoneId ZONE = ZoneId.of("Asia/Ho_Chi_Minh");
}
