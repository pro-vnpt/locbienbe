package com.trandung.locbien.controller;

import com.trandung.locbien.dto.ClassifyDTO;
import com.trandung.locbien.dto.ResponseBodyDTO;
import com.trandung.locbien.entity.BienSoEntity;
import com.trandung.locbien.service.PlateService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/plates")
@AllArgsConstructor
public class PlateController {
    private final PlateService plateService;

    @GetMapping
    public ResponseEntity<ResponseBodyDTO<BienSoEntity>> getAll() {
        List<BienSoEntity> result = plateService.getAll();
        ResponseBodyDTO<BienSoEntity> response = new ResponseBodyDTO<>(result, "200", "OK", result.size());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/paging/")
    public ResponseEntity<ResponseBodyDTO<BienSoEntity>> getAllPaging(Pageable pageable) {
        Page<BienSoEntity> result = plateService.getAllPaging(pageable);
        ResponseBodyDTO<BienSoEntity> response = new ResponseBodyDTO<>(pageable, result, "200", "OK");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping("/classify")
    public ResponseEntity<ResponseBodyDTO<ClassifyDTO>> classify(@RequestBody List<String> plates) {
        List<ClassifyDTO> result = plateService.classify(plates);
        ResponseBodyDTO<ClassifyDTO> response = new ResponseBodyDTO<>(result, "200", "OK", result.size());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
