package com.trandung.locbien;

import com.trandung.locbien.service.MailService;
import com.trandung.locbien.service.PlateService;
import com.trandung.locbien.service.VpaService;
import jakarta.annotation.PostConstruct;
import org.jobrunr.scheduling.JobScheduler;
import org.jobrunr.scheduling.cron.Cron;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import java.util.TimeZone;

@SpringBootApplication
public class LocBienApplication {
    @Autowired
    private JobScheduler jobScheduler;

    @Autowired
    private VpaService vpaService;

    @Autowired
    private MailService mailService;

    @Autowired
    private PlateService plateService;

    public static void main(String[] args) {
        SpringApplication.run(LocBienApplication.class, args);
    }

    @PostConstruct
    public void scheduleRecurrently() {
        TimeZone.setDefault(TimeZone.getTimeZone("Asia/Ho_Chi_Minh"));
        jobScheduler.scheduleRecurrently(Cron.daily(8), () -> vpaService.syncBienSoBidToday());
        jobScheduler.scheduleRecurrently(Cron.daily(6, 30), () -> vpaService.syncBienSoExpireNext3Day());
        jobScheduler.scheduleRecurrently(Cron.daily(7, 00), () -> mailService.sendToAllExpiredNext3Day());
        // jobScheduler.scheduleRecurrently(Cron.daily(9, 0), () -> mailService.sendToAllBidToday());
        jobScheduler.scheduleRecurrently(Cron.daily(17, 50), () -> vpaService.syncKetQua());
        jobScheduler.scheduleRecurrently(Cron.daily(18), () -> mailService.sendToAllResultToday());
    }

    @Bean
    public CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.addAllowedOrigin("*");
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");
        source.registerCorsConfiguration("/**", config);
        return new CorsFilter(source);
    }
}
