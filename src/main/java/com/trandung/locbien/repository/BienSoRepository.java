package com.trandung.locbien.repository;

import com.trandung.locbien.entity.BienSoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.List;

@Repository
public interface BienSoRepository extends JpaRepository<BienSoEntity, String> {
    List<BienSoEntity> findAllByTinhAndAuctionStartTimeBetween(String tinh, ZonedDateTime start, ZonedDateTime end);

    List<BienSoEntity> findAllByAuctionStartTimeBetween(ZonedDateTime start, ZonedDateTime end);

    List<BienSoEntity> findAllByRegisterToTimeBetween(ZonedDateTime start, ZonedDateTime end);

    List<BienSoEntity> findAllByGiaIsNullAndAuctionStartTimeBetween(ZonedDateTime start, ZonedDateTime end);
}
